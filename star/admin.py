# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import User,Call,Follow,Celebrity

admin.site.register(User)
admin.site.register(Celebrity)
admin.site.register(Call)
admin.site.register(Follow)

