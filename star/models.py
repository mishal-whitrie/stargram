# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User


class User(models.Model):
    user = models.ForeignKey(User)
    username = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.CharField(max_length=40)
    rating = models.IntegerField(default=0)


class Celebrity(models.Model):
    celebrity = models.ForeignKey(User)
    bio = models.CharField(max_length=100)
    calling_rate = models.FloatField(max_length=10)
    verified_at = models.DateTimeField(auto_now=True)


class Call(models.Model):
    user = models.ForeignKey(User)
    celebrity = models.ForeignKey(Celebrity)
    requested_at = models.DateTimeField(auto_now=True)
    start_time = models.DateTimeField(auto_now=True)
    disconnected_at = models.DateTimeField(auto_now=True)


class Follow(models.Model):
    user = models.ForeignKey(User)
    celebrities = models.ManyToManyField(Celebrity)

